<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaComposer\Utility;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_composer
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Composer
{
    protected static $contents;

    static public function fetch()
    {
        foreach (self::getLoadedExtensions() as $extKey => $extPath) {
            $autoloadFile = self::getPath($extKey, true);
            if (file_exists($autoloadFile)) {
                @require_once($autoloadFile);
            }
        }
    }

    /**
     * @param string $path
     */
    static public function install()
    {
        self::update(null, 'install');
    }

    /**
     * @param string $path
     * @param string $action
     * @return string
     */
    static public function update($extKey = null, $action = 'update')
    {
        $contents = '';
        if ($extKey) {
            self::$contents = '';
            $extPath = ExtensionManagementUtility::extPath($extKey);
            if (file_exists($extPath . 'ext_composer.json')) {
                $composerPath = self::getPath($extKey, false, true);
                \putenv('COMPOSER_HOME=' . self::getPath($extKey) . '');

                if (file_exists($composerPath . '/composer.json')) {
                    @unlink($composerPath . '/composer.json');
                }
                if (file_exists($composerPath . '/composer.lock')) {
                    @unlink($composerPath . '/composer.lock');
                }
                file_put_contents($composerPath . '/composer.json', GeneralUtility::getUrl($extPath . 'ext_composer.json'));

                $hideIframeTop = 89 + 29 + 22;
                echo '<pre id="preconsole" style="word-wrap: break-word; white-space: pre-wrap; margin-top: ' . $hideIframeTop . 'px">';

                self::exec('cd ' . $composerPath . '; curl -s https://getcomposer.org/installer | php -d apc.enable_cli=Off');
                self::exec('cd ' . $composerPath . '; chmod -R 777; rm vendor -Rf;', false);
                self::exec('cd ' . $composerPath . '; php -d apc.enable_cli=Off composer.phar config -g secure-http false');
                self::exec('cd ' . $composerPath . '; php -d apc.enable_cli=Off composer.phar ' . $action . ' --no-interaction');
                self::exec('cd ' . $composerPath . '; rm -Rf cache', false);

                if (!is_dir($composerPath . '/vendor')) {
                    self::$contents .= 'composer install failed' . "\n";
                    self::$contents .= '$ cd ' . $composerPath . '; php -d apc.enable_cli=Off composer.phar ' . $action . ' --no-interaction';
                } else {
                    @unlink($composerPath . '/composer.phar');
                }
                echo '</pre>';

                $contents = self::$contents;
            }
        } else {
            foreach (self::getLoadedExtensions() as $extKey => $extPath) {
                $contents .= self::update($extKey, $action);
            }
        }

        return $contents;
    }

    /**
     * @return array
     */
    static public function getExtensions()
    {
        $extensions = [];
        foreach (self::getLoadedExtensions() as $extKey => $extPath) {
            if (ExtensionManagementUtility::isLoaded($extKey) && isset($GLOBALS['TYPO3_LOADED_EXT'][$extKey])
                && is_dir($extPath) && file_exists($extPath . 'ext_composer.json')
            ) {
                $extInfo = $GLOBALS['TYPO3_LOADED_EXT'][$extKey];
                $extInfo['ext_key'] = $extKey;
                $extInfo['ext_composer'] = ExtensionManagementUtility::extPath($extKey, 'ext_composer.json');
                $extInfo['ext_composer_vendor'] = ExtensionManagementUtility::extPath($extKey, 'Resources/Private/Vendor/Composer/');

                $EM_CONF = [];
                @include(ExtensionManagementUtility::extPath($extKey, 'ext_emconf.php'));
                $emConf = is_array($EM_CONF) ? $EM_CONF : [null];
                $extInfo['em_conf'] = array_shift($emConf);

                $extensions[$extKey] = $extInfo;
            }
        }
        ksort($extensions);

        return $extensions;
    }

    /**
     * @param string $command
     * @param boolean $showOutput
     */
    static private function exec($command, $showOutput = true)
    {
        if (function_exists('popen')) {
            if ($showOutput) {
                error_reporting(E_ERROR | E_PARSE);
                flush();
                ob_end_flush();
                @ini_set('output_buffering', '0');
                ob_implicit_flush(true);
                ob_start();
                @header('Content-Type: text/event-stream');
                @header('Cache-Control: no-cache');
                ob_end_clean();

                $showCommand = "\n" . '$ ' . preg_replace('/cd .*? /ism', '', $command) . "\n";
                if (trim($showCommand)) {
                    self::$contents .= $showCommand;
                    echo $showCommand;
                    flush();
                    ob_end_flush();
                }
            }

            $proc = popen($command . ' 2>&1', 'r');
            while (!feof($proc)) {
                $newContent = fread($proc, 4096);

                if ($showOutput) {
                    $newContent = preg_replace('/#!\/usr\/bin\/env php/ism', '', $newContent);
                    $newContent = preg_replace('/All settings correct for using Composer/ism', '', $newContent);
                    $newContent = preg_replace('/(Composer successfully installed) to: .*?phar/ism', '$1', $newContent);
                    $newContent = preg_replace('/Use it: php composer\.phar/ism', '', $newContent);

                    if (trim($newContent)) {
                        $newContent = trim($newContent) . "\n";

                        self::$contents .= $newContent;
                        echo $newContent;
                        flush();
                        ob_end_flush();
                    }
                }
            }
        } else {
            exec($command . ' 2>&1', $output);
            if ($showOutput) {
                echo implode("\n", $output) . "\n";
            }
        }
    }

    /**
     * @param string $extKey
     * @param boolean $autoloadFile
     * @return string
     */
    static private function getPath($extKey, $autoloadFile = false, $createIfNotExists = false)
    {
        $path = ExtensionManagementUtility::extPath($extKey, 'Resources/Private/Vendor/Composer');
        if ($createIfNotExists) {
            self::ensureDirExists($path);
        }

        return $path . ($autoloadFile ? '/vendor/autoload.php' : '');
    }

    /**
     * @param string $path
     */
    static private function ensureDirExists($path)
    {
        if ($path && !is_dir($path)) {
            self::ensureDirExists(dirname($path));
            @mkdir($path);
        }
    }

    /**
     * @return array
     */
    static private function getLoadedExtensions()
    {
        $extensions = [];
        if (array_key_exists('TYPO3_LOADED_EXT', $GLOBALS)) {
            foreach ($GLOBALS['TYPO3_LOADED_EXT'] as $extKey => $extInfo) {
                $extensions[$extKey] = ExtensionManagementUtility::extPath($extKey);
            }
        } else {
            $typo3Configuration = include(PATH_site . 'typo3conf/LocalConfiguration.php');

            foreach ($typo3Configuration['EXT']['extListArray'] as $extKey) {
                $extensions[$extKey] = PATH_site . 'typo3conf/ext/' . $extKey . '/';
            }
        }

        $loadedExtensions = [];
        foreach ($extensions as $extKey => $extPath) {
            if (is_dir($extPath) && file_exists($extPath . 'ext_composer.json')) {
                $loadedExtensions[$extKey] = $extPath;
            }
        }

        return $loadedExtensions;
    }
}