<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use IMIA\ImiaComposer\Utility\Composer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;

class ext_update
{
    /**
     * @var array
     */
    protected $allVendors = [];

    /**
     * @var array
     */
    protected $vendorConflicts = [];

    /**
     * @return bool
     */
    public function access()
    {
        return true;
    }

    /**
     * @return string
     */
    public function main()
    {
        $content = '';
        if (GeneralUtility::_GP('modal')) {
            $content = '<style type="text/css">.module-body {padding-left: 0; padding-right: 0; padding-bottom: 0}</style>';
        }

        $extensions = Composer::getExtensions();
        $params = GeneralUtility::_GP('composer');
        if (!is_array($params)) {
            $params = [
                'action' => 'index',
            ];
        }
        $requestUri = preg_replace('/&composer[^&]+/ism', '', $_SERVER['REQUEST_URI']);

        switch ($params['action']) {
            case 'update':
                if ($params['extKey']) {
                    $extension = $extensions[$params['extKey']];
                    $content .=
                        '<h2>' . $extension['em_conf']['title'] . '</h2>' .
                        '<pre style="margin: 24px 0 12px; font-size: 12px; word-wrap: break-word;">' . $this->updateExtension($params['extKey']) . '</pre><br>' .
                        '<a href="' . $requestUri . '" class="t3-button btn btn-default">' .
                        $this->translate('update.back') .
                        '</a>
                        <script type="text/javascript">document.getElementById("preconsole").remove();</script>';
                }
                break;
            case 'index':
            default:
                foreach ($extensions as $extension) {
                    $vendors = '';

                    $config = null;
                    $lock = null;
                    $lockTime = null;
                    if (file_exists($extension['ext_composer'])) {
                        $config = @json_decode(@GeneralUtility::getUrl($extension['ext_composer']), true);
                    }

                    $installedPackages = [];
                    if (file_exists($extension['ext_composer_vendor'] . 'composer.lock')) {
                        $lock = @json_decode(@GeneralUtility::getUrl($extension['ext_composer_vendor'] . 'composer.lock'), true);
                        $lockTime = @filemtime($extension['ext_composer_vendor'] . 'composer.lock');
                    }

                    if (is_array($lock) && is_array($lock['packages'])) {
                        foreach ($lock['packages'] as $package) {
                            $installedPackages[$package['name']] = $package;
                        }
                    }

                    if ($config && is_array($config) && is_array($config['require'])) {
                        $vendorPackages = [];
                        foreach ($config['require'] as $vendor => $version) {
                            if (array_key_exists($vendor, $installedPackages)) {
                                $vendorPackages[$vendor] = [$installedPackages[$vendor], $version];
                                $this->addVendor($vendor, $version);
                                unset($installedPackages[$vendor]);
                            } else {
                                $vendorPackages[$vendor] = [null, $version];
                                $this->addVendor($vendor, $version);
                            }
                        }

                        foreach ($vendorPackages as $vendor => $vendorPackageArr) {
                            $vendorPackage = $vendorPackageArr[0];
                            $version = $vendorPackageArr[1];
                            $vendors .= '<li' . (!$vendorPackage ? ' style="color: red"' : ($this->compareVersions($vendorPackage['version'], $version)
                                    ? ' style="color: green"' : ' style="color: orange"')) . ($vendorPackage['time'] ? ' title="' . $vendorPackage['time'] . '"' : '') . '>' .
                                $vendor . ' (' . ($vendorPackage && $vendorPackage['version'] != $version ? $vendorPackage['version'] : $version) . ')' .
                                $this->getAdditionalVendors($vendorPackage, $installedPackages) . '</li>';
                        }

                        foreach ($installedPackages as $vendor => $additionalPackage) {
                            $vendors .= '<li><small>' .
                                $additionalPackage['name'] . ' (' . $additionalPackage['version'] . ')' .
                                '</small></li>';
                        }
                    }
                    if ($vendors) {
                        $vendors = '<ul style="margin: 0; padding: 0 0 0 18px">' . $vendors . '</ul>';
                    }

                    $content .=
                        '<div style="width: 50%; vertical-align: top; display: inline-block">' .
                        '<p style="margin: 0 0 12px; padding: 0;">' .
                        '<strong>' . ($extension['em_conf']['title'] ?: $extension['ext_key']) . ($lockTime
                            ? ' <small style="color: ' . ($lockTime > time() - 60 * 60 * 24 * 30 ? 'green' : 'orange') . '">(' .
                            date('d.m.Y H:i', $lockTime) . ')</small>' : '') . '</strong>' .
                        '</p>' .
                        $vendors .
                        '<p style="margin-top: 6px; padding: 0;">' .
                        '<a href="' . $requestUri . '&composer[action]=update&composer[extKey]=' . $extension['ext_key'] . '" class="t3-button btn btn-default">' .
                        '<span class="t3js-icon icon icon-size-small icon-state-default icon-actions-refresh"><span class="icon-markup"><img src="/typo3/sysext/core/Resources/Public/Icons/T3Icons/actions/actions-refresh.svg" width="16" height="16"></span></span> ' . $this->translate('update.update') .
                        '</a>' .
                        '</p>' .
                        '</div>';
                }

                if (count($this->vendorConflicts)) {
                    $flashMessageService = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessageService');
                    foreach ($this->vendorConflicts as $conflict) {
                        foreach ($conflict as $vendor => $versions) {
                            $flashMessage = GeneralUtility::makeInstance('TYPO3\CMS\Core\Messaging\FlashMessage',
                                'Vendor: ' . $vendor . ' has different versions: ' . $versions[0] . ' / ' . $versions[1],
                                'Vendor conflict',
                                FlashMessage::WARNING,
                                true
                            );
                            $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);
                        }
                    }

                    $content = $flashMessageService->getMessageQueueByIdentifier()->renderFlashMessages() . '<br><br>' . $content;
                }

                break;
        }

        return $content;
    }

    /**
     * @param string $vendor
     * @param string $version
     */
    protected function addVendor($vendor, $version)
    {
        if (array_key_exists($vendor, $this->allVendors)
            && !$this->compareVersions($this->allVendors[$vendor], $version)
        ) {
            $this->vendorConflicts[] = [
                $vendor => [
                    $this->allVendors[$vendor],
                    $version,
                ],
            ];
        }
        $this->allVendors[$vendor] = $version;
    }

    /**
     * @param array $vendorPackage
     * @param array $installedPackages
     * @return string
     */
    protected function getAdditionalVendors($vendorPackage, &$installedPackages)
    {
        if ($vendorPackage && $vendorPackage['require'] && count($vendorPackage['require']) > 0) {
            $additionalVendors = '';
            $additionalPackages = [];
            foreach ($vendorPackage['require'] as $vendor => $vendorVersion) {
                if (array_key_exists($vendor, $installedPackages)) {
                    $additionalPackages[] = $installedPackages[$vendor];
                    $this->addVendor($vendor, $vendorVersion);
                    unset($installedPackages[$vendor]);
                }
            }

            foreach ($additionalPackages as $additionalPackage) {
                $additionalVendors .= '<li>' .
                    '<small>' .
                    $additionalPackage['name'] . ' (' . $additionalPackage['version'] . ')' .
                    '</small>' .
                    $this->getAdditionalVendors($additionalPackage, $installedPackages) .
                    '</li>';
            }

            if ($additionalVendors) {
                return '<ul style="padding-left: 12px; margin-bottom: 0;">' . $additionalVendors . '</ul>';
            } else {
                return '';
            }
        }

        return '';
    }

    /**
     * @param string $version
     * @param string $versionRequirement
     * @return bool
     */
    protected function compareVersions($version, $versionRequirement)
    {
        if ($version == $versionRequirement) {
            $equals = true;
        } elseif (($versionRequirement == 'dev-master' || $versionRequirement == '*') && $version) {
            $equals = true;
        } else {
            $version = preg_replace('/^v/', '', $version);
            $versionParts = explode('.', $version);
            $versionRequirement = preg_replace('/^v/', '', $versionRequirement);
            $versionRequirementParts = explode('.', $versionRequirement);

            $equals = true;
            foreach ($versionParts as $key => $part) {
                $part = str_replace('~', '', $part);
                $reqPart = str_replace('~', '', $versionRequirementParts[$key]);

                if ($part && $reqPart && $reqPart != $part) {
                    if (strpos($reqPart, 'x') === false && strpos($reqPart, '*') === false
                        && strpos($reqPart, 'dev') === false && strpos($part, 'x') === false
                        && strpos($part, '*') === false && strpos($part, 'dev') === false
                    ) {
                        $equals = false;
                        break;
                    }
                }
            }
        }

        return $equals;
    }

    /**
     * @param $extKey
     * @return string
     */
    public function updateExtension($extKey)
    {
        $consoleOutput = Composer::update($extKey);

        $output = str_replace([
            '[0m',
            '[0;32m',
            '[33;33m',
        ], [
            '</span>',
            '<span style="color: green">',
            '<span style="color: #0063FF">',
        ], $consoleOutput);

        $output = str_replace("\r", '', $output);
        while (strpos($output, "\n\n\n")) {
            $output = str_replace("\n\n\n", "\n\n", $output);
        }

        return trim($output);
    }

    /**
     * Translate by key
     *
     * @param string $key
     * @param array $arguments
     * @return string
     */
    protected function translate($key, $arguments = [])
    {
        return (string)LocalizationUtility::translate($key, 'imia_composer', $arguments);
    }
}
